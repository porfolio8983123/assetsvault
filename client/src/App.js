import "./App.css";
import LandingPageBody from "./components/LandingPageBody";
import { createBrowserRouter,createRoutesFromElements,Route,RouterProvider,Navigate } from "react-router-dom";
import RootLayouts from "./components/layouts/RootLayouts";
import AboutUs from "./components/landingPage/AboutUs";
import HowItWorks from "./components/landingPage/HowItWorks";
import PageNotFound from "./components/error/PageNotFound";
import HomeLayout from "./components/layouts/HomeLayout";
import TwoDAssets from "./components/home/assets/TwoDAssets";
import { lazy, Suspense, useState,useEffect } from "react";
import PreviewLayout from "./components/layouts/PreviewLayout";
import Overview from "./components/home/preview/Overview";
import FileContent from "./components/home/preview/FileContent";
import Feedback from "./components/home/preview/Feedback";
import Rating from "./components/home/preview/Rating";
import Profile from "./components/user/Profile";
import ForgotPassword from "./components/user/ForgotPassword";
import OTPVerification from "./components/layouts/OTPVerification";

const ThreeDAssets = lazy(() => import('./components/home/assets/ThreeDAssets'))

function App(props) {

  const [isAuthenticated,setIsAuthenticated] = useState(false);

  const setAuth = (boolean) => {
    setIsAuthenticated(boolean);
  }

  async function isAuth() {
    try {
      const response = await fetch("https://csassetsvault.onrender.com/auth/is-verified",{
        method:'GET',
        headers: {token: localStorage.token}
      });
      const parseRes = await response.json();
      parseRes === true ? setIsAuthenticated(true):setIsAuthenticated(false)
    } catch (error) {
      console.log("is Fetching")
      console.log(error.message);
    }
  }

  useEffect(() => {
    isAuth();
  })

  const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path = "/">
          <Route path="/" element = {!isAuthenticated?<RootLayouts setAuth = {setAuth}/>:<Navigate to = "/home"/>}>
            <Route path = "/" element = {<LandingPageBody/>}/>
            <Route path="aboutus" element = {<AboutUs/>}/>
            <Route path="howitworks" element = {<HowItWorks/>}/>
          </Route>
  
          <Route path="home" element = {!isAuthenticated?<Navigate to = "/"/>:<HomeLayout setAuth = {setAuth}/>}>
            <Route index element = {<ThreeDAssets/>}/>
            <Route path = "2d" element = {<TwoDAssets/>}/>
          </Route>
  
          <Route path="preview/:id" element = {!isAuthenticated?<Navigate to = "/"/>:<PreviewLayout setAuth = {setAuth}/>}>
            <Route index element = {<Overview/>}/>
            <Route path = "fileContent" element = {<FileContent/>}/>
            <Route path = "feedback" element = {<Feedback/>}/>
            <Route path = "rating" element = {<Rating/>}/>
          </Route>
  
          <Route path = "profile" element = {!isAuthenticated?<Navigate to = "/"/>: <Profile setAuth = {setAuth}/>}>
  
          </Route>

          <Route path = "otpverification" element = {<OTPVerification/>}>
            
          </Route>

          <Route path = "forgotpassword" element = {<ForgotPassword/>}>
  
          </Route>
  
          <Route path="*" element = {<PageNotFound/>}/>
        </Route>
    )
  )

  return (
    <Suspense fallback = {<div>Loading...</div>}>
      <RouterProvider router = {router}/>
    </Suspense>
    
  );
}

export default App;
