import { Link, NavLink, Outlet } from "react-router-dom";
import '../../components/home/assets/threed.css';
import { useState, useEffect } from "react";
import '../../components/font.css';

export default function PreviewHeader(props) {

    const [userName,setUserName] = useState("");
    const [image,setImage] = useState();


    const getUserData = async () => {
        const response = await fetch("https://csassetsvault.onrender.com/auth/user",{
            method:'GET',
            headers:{token:localStorage.token}
        })

        const parseRes = await response.json();
        console.log(parseRes);
        setUserName(parseRes.user_name);
        setImage(parseRes.profile);

        if (parseRes.encode) {
            const binaryData = await atob(parseRes.encode)
            const byteArray = new Uint8Array(binaryData.length)

            for (let i = 0; i < binaryData.length; i++) {
                byteArray[i] = binaryData.charCodeAt(i);
            }

            const blob = new Blob([byteArray],{type:'image/png'});
            const url = URL.createObjectURL(blob);
            console.log("url",url);
            setImage(url);
        }
    }

    useEffect(()=> {
        getUserData();
        return;
    },[])

    return(
        <div className = "mb-3">
            <header style={{position:"-webkit-sticky", position:'sticky',top:0,zIndex:10}}>
                <section className="header-main border-bottom bg-dark">
                    <div className="container-fluid">
                    <div className="row p-2 pt-3 pb-3 d-flex align-items-center">
                        <div className="col-md-2">
                            <Link to="/home" className='text-decoration-none text-white'><h1 style={{fontFamily:"customFont"}}><span style={{color:"#FE4C00"}}>Assets</span>Vault</h1></Link>
                        </div>
                        <div className="col-md-8">
                       
                        </div>
                        
                        <div className="col-md-2">
                            <div className="d-flex d-none d-md-flex flex-row align-items-center ms-4">
                                <div className="d-flex justify-content-center align-items-center ms-2">
                                    <div>
                                        <span className="qty text-white">{userName}</span>
                                    </div>
                                    <Link to = {{
                                        pathname: `/profile`
                                    }}>
                                        <div className="ms-3" style={{width:50,height:50, borderRadius:50}}>
                                        {!image && 
                                            <img src = "https://icons-for-free.com/download-icon-person+profile+user+icon-1320184018411209468_512.png" style={{width:50,height:50}}/>
                                        }
                                        { image && 

                                            <img src = {image} style={{width:50,height:50, borderRadius:50}}/>
                                        
                                        }
                                        </div>
                                    </Link>
                                </div>    
                            </div>           
                        </div>
                    </div>
                    </div> 
                </section>

                <nav className="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
                <div className="container">
                    <a className="navbar-brand d-md-none d-md-flex text-white" href="#">Categories</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        
                    </ul>
                    </div>
                </div>
            </nav>
            </header>
        </div>
    )
}