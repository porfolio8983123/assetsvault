import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"

export default function FileContent(props) {

    const {id} = useParams();
    const [models, setModels] = useState([]);

    async function getData() {
        await fetch(`https://csassetsvault.onrender.com/auth/model/${id}`)
        .then(response => response.json())
        .then(data => setModels(data))
        .catch(error => console.error(error))
    }

    useEffect( () => {
        getData();
    },[])

    console.log(models.engine)


    return(
        <div className="d-flex align-items-center">
            <h6>Total file size: {models.filesize?(models.filesize / 1048576).toFixed(2):
                <div class="spinner-border text-success" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
            } MB</h6>
        </div>
    )
}