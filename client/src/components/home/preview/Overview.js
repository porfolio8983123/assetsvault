import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"

export default function Overview(props) {

    const {id} = useParams();
    const [models, setModels] = useState([]);

    async function getData() {
        await fetch(`https://csassetsvault.onrender.com/${id}`)
        .then(response => response.json())
        .then(data => setModels(data))
        .catch(error => console.error(error))
    }

    useEffect( () => {
        getData();
    },[])


    return(
        <div>
            <h3>Description</h3>
            <h6 className="mt-4">{models.overview ? models.overview:
                <div class="spinner-border text-success" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
            }</h6>
            <div className="mt-5">
                <h5>Model Details</h5>
            </div>
            <div className="text-white d-flex align-items-center">
                Published at :  {models.created_at ? new Date(models.created_at).toDateString():<div class="spinner-border text-success" role="status">
  <span class="visually-hidden"></span>
</div>}
            </div>
        </div>
    )
}