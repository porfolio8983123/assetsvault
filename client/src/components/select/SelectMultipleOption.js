import React, { useState } from 'react'
import Select from 'react-select'

const options = [
  { value: 'Unity', label: 'Unity' },
  { value: 'Unreal Engine', label: 'Unreal Engine' },
  { value: 'Godot', label: 'Godot' },
  { value: 'Game Maker Studio', label: 'Game Maker Studio' },
  { value: 'Scratch', label: 'Scratch' },
  { value: 'RPG Maker', label: 'RPG Maker' }
]

export default function SelectMultipleOption(props) {

  const handleSelectChange = (selected) => {
    props.handleSelection(selected);
  }

  return (
    <Select
      closeMenuOnSelect={false}
      value = {props.yourSelection}
      onChange={handleSelectChange}
      isMulti
      options={options}
    />
  );
}