const jwt = require("jsonwebtoken")
require("dotenv").config();

module.exports = async (req,res, next) => {
    try {
        // destructure
        const jwtToken = req.header('token');
        if (!jwtToken) {
            return res.status(403).json("Not Authorized!");
        }

        const payload = jwt.verify(jwtToken,process.env.jwtSecret);

        console.log("getting user")

        req.user = payload.user;

        console.log("authorization ", req.user)

    } catch (error) {
        console.error(error.message);
        return res.status(403).json("Not Authorized!")
    }
    next();
}