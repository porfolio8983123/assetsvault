const Pool = require('pg').Pool;

const pool = new Pool({
    // user: "postgres",
    // host: "localhost",
    // database: "assetsvault",
    // password: "dalboys66",
    // port: 5432
    // connectionString: 'postgres://assetsvault_user:YLcnSrmzQXLcFyunRZaHAy51nKzS7h2n@dpg-ci3bqrak728i8te1hu70-a.oregon-postgres.render.com/assetsvault?ssl=true'
    connectionString: 'postgres://assetsvault_user:YLcnSrmzQXLcFyunRZaHAy51nKzS7h2n@dpg-ci3bqrak728i8te1hu70-a.oregon-postgres.render.com/assetsvault?ssl=true',
  ssl: {
    rejectUnauthorized: false // Only if you encounter certificate verification issues
  }
})

pool.connect()
.then(()=>{
    console.log("Connected To Database!");
})
.catch((error) => {
    console.log(error.message);
})

module.exports = pool;